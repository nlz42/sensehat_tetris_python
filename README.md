# Senshat on RaspberryPi Tetris with Python  #

Just the game Tetris for the RaspberryPi with a SensHat on the Top.

### Start it ###

* python snake.py

### Control it ###

* use the joystick to control the snake
* after game over (you looser), press the middle of joystick to restart game.

### Hava fun with it ¯\_(ツ)_/¯ ###
